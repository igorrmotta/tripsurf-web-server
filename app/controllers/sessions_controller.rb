class SessionsController < ApplicationController
  def create
    if user = User.authenticate(params[:email], params[:password])
      session[:user_id] = user.id
      redirect_to dashboard_path, :notice => 'Logged in successfully'
    else
      redirect_to root_path, :alert => 'Invalid login/password combination'
    end
  end

  def destroy
    reset_session
    redirect_to root_path, :notice => 'You successfully logged out'
  end
end
