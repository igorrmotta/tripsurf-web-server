class DashboardController < ApplicationController
  def index
    if current_user.is_a? User
      render :action => 'show'
    end
  end

  def show

  end
end
