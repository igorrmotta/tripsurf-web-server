Rails.application.routes.draw do

  root to: 'dashboard#index'

  get '/dashboard' => 'dashboard#index', :as => 'dashboard'
  get '/login' => 'sessions#new', :as => 'login'
  get '/logout' => 'sessions#destroy', :as => 'logout'
  resource :sessions
  resources :users
  resource :dashboard
end
