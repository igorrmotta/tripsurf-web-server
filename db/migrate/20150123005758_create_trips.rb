class CreateTrips < ActiveRecord::Migration
  def self.up
    create_table :trips do |t|
      t.boolean :isPrivate
      t.integer :maxUsers
      t.integer :minUsers
      t.date :beginDate
      t.date :endDate

      t.timestamps null: false
    end
  end

  def self.down
    drop_table :trips
  end
end
